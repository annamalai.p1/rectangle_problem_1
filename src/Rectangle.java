public class Rectangle implements Shapes {
    int length;
    int breadth;

    public Rectangle(int length, int breadth) {
        this.length = length;
        this.breadth = breadth;
    }

    @Override
    public int area() {
        return this.length * this.breadth;
    }

    @Override
    public int perimeter() {
        return 2 * (length + breadth);
    }
}








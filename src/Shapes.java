public interface Shapes {
    int area();
    int perimeter();
}


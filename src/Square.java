
public class Square implements Shapes {
    int side;

    public Square(int side) {
        this.side = side;
    }

    public int perimeter() {
        return 4 * side;

    }

    public int area() {

        return side * side;
    }


}

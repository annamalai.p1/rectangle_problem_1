import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquareTest {


    @Test
    void shouldReturnPerimeterAsFourtyWhenSideIsTen() {
        Square square = new Square(10);

        int perimeterOfSquare = square.perimeter();

        assertEquals(40, perimeterOfSquare);
    }

    @Test
    void shouldReturnAreaAsTwentyFiveWhenSideIsFive(){
        Square square = new Square(5);

        int areaOfSquare = square.area();

        assertEquals(25,areaOfSquare);
    }

}


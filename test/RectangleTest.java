import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RectangleTest {

    @Test
    void shouldReturnAreaOfRectangleWhenLengthAndBreadthAreGiven() {
        Rectangle rectangle=new Rectangle(7,8);

        int areaOfRectangle=rectangle.area();

        assertEquals(56,areaOfRectangle);
    }
    @Test
    void shouldReturnPerimeterOfRectangleWhenLengthAndBreadthAreGiven() {
        Rectangle rectangle=new Rectangle(4,3);

        int perimeterOfRectangle=rectangle.perimeter();

        assertEquals(14,perimeterOfRectangle);
    }

    @Test
    void shouldReturnPerimeterAsThirtyWhenLengthIsFiveAndBreadthIsTen() {
        Rectangle rectangle = new Rectangle(5, 10);

        int perimeterOfRectangle = rectangle.perimeter();

        assertEquals(30, perimeterOfRectangle);
    }


}
